//importing libraries
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
const fs = require('fs')
const HummusRecipe = require('hummus-recipe');
const path = require('path');
const bcrypt = require('bcrypt-nodejs')
var CryptoJS = require('crypto-js');
var htmlPdfTemplate = fs.readFileSync(path.join(__dirname, 'views/pdf.ejs'), 'utf8');
var sampleHtmlTemplate = fs.readFileSync(path.join(__dirname, 'views/statement.ejs'), 'utf8');
var summaryHtmlTemplate = fs.readFileSync(path.join(__dirname, 'views/summary.ejs'), 'utf8');
//const pdf = require('html-pdf');
var currencyFormatter = require('currency-formatter');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
var cron = require('node-cron');
const shell = require('shelljs');
const pdf = require('html-pdf');
var async = require('async');
let ejs = require('ejs');
// importing nodemailer
var nodemailer = require('nodemailer');
const merge = require('easy-pdf-merge');


// server
const directory = '/home/t24smart/pdf/';
const pdfDownloadDir = '/opt/ss/smart-statement/public/pdfDownload/';


// local
// const directory = '/home/ubuntu/smart-statement/pdf/';
// const pdfDownloadDir = '/home/ubuntu/smart-statement/pdfDownload/';

var ip = require("ip");


// from address for mail
//server
//var fromAdress = "Bank of Abyssinia Statement <SMARTSTATEMENT.SMTP@bankofabyssinia.com>";

//local
var fromAdress = "Smart Statement Bank of Abyssinia<smart-statement@wrkwth.com>"

var options = {

    "type": "pdf",
    "format": "A4",

    "header": {
        "height": '10mm'
    },
    "footer": {
        "height": "10mm",
    },
    "orientation": "portrait",
    "zoomFactor": "1",
    "border": {
        "top": '0',
        "right": "3mm",
        "left": "5mm",
        "bottom": "1mm"
    },
    "timeout": 600000
};

// nodemailer SMTP 
// var options = {
//     host: "mail.bankofabyssinia.com",
//     port: 25,
//     ignoreTLS: true,
//     //secure: true, // use TLS
//     auth: {
//       user: "SMARTSTATEMENT.SMTP@bankofabyssinia.com",
//       pass: "Welcome2boa"
//     },
//     tls: {
//       // do not fail on invalid certs
//       rejectUnauthorized: false
//     }
//   }
// var smtpTransport = nodemailer.createTransport(options)

//smtp configuration using aws
var smtpTransport = nodemailer.createTransport({
    host: "email-smtp.us-east-1.amazonaws.com",
    port: 465,
    secure: true,
    auth: {
        user: "AKIAIAJSIT2B3VM6ML2A",
        pass: "BP3Pb7s5148Q3rHJ2jbLCR5sZzGq2/T7yTEyRQ9WKkvS"
    },
    tls: {
        rejectUnauthorized: false
    }
});

// function to send mail
function send_mail(to, subject, body, html, cb) {
    var obj = {
        from: fromAdress, // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        text: body, // plain text body
        html: html // html body
    };

    // if (size < 9) {
    //   obj.attachments = attachments;
    // }

    smtpTransport.sendMail(
        obj,
        function(err, data) {

            cb(err, data);
        });
};
// importing elastic search
const { Client } = require('elasticsearch');

const client = new Client({
    //host: ['http://localhost:9200'],
    host: ['http://52.187.13.68:9200/'],
    requestTimeout: 60000
    //log: 'trace'
});

// client ping
client.ping({
    requestTimeout: 3000
}, function(error) {
    if (error) {
        console.trace('Error:', error);
    }
    else {
        console.log('Elasticsearch Connected!');
    }
    // on finish
    //client.close();
});

//banklogo

const bankLogo = "http://s1.shzy.in/bs/bankLogo.png"

// server
const IP = "http://196.188.92.168/bankStatement/Id/"

// local
//const IP = "http://34.204.87.96:8013/bankStatement/Id/"


//running the express 
var app = express();

//Integrating cors and bodyParser plugins to express
app.use(cors());
app.use(bodyParser.json())

// app.use(function(req, res, next){
//     req.setTimeout(10000) // no timeout for all requests, your server will be DoS'd
//     next()
//   })
// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(express.static('public'));

app.get('/test', function(req, res) {
    console.log("WORKING...");
    res.send("WORKING...")
});

// app.get('/ss', function(req, res) {
//     console.log("WORKING...");
//     res.render("statement-bkp");
// });
// app.get('/summary', function(req, res) {
//     console.log("Summary...");
//     res.render('bkpsummary');
// });

app.get('/tempfile/:id', function(req, res) {
    let id = req.params.id;
    //console.log("inside /tempfile/:id api")
    //console.log("id", id)
    //console.log("__dirname", __dirname)
    // console.log("server.address() ", req.get('host'));
    //console.log("tempfile", id);
    // res.download("/home/t24smart/7146515_01_AUG_2019-06_AUG_2019_39.pdf");
    
    // local
    //res.download(pdfDownloadDir + id);
    
    //server
     res.download(__dirname + '/public/pdfDownload/' + id);
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});



// function to validate json 
function IsJsonString(str) {
    try {
        JSON.parse(str);
    }
    catch (e) {
        console.log("JSON ERROR", e)
        return false;
    }
    return true;
}

// function to get pdf file size
function getFileSize(filePath) {
    var stats = fs.statSync(filePath);
    var size = stats["size"];
    var fileSizeInMegabytes = size / 1000000.0
    return fileSizeInMegabytes;
}

//cron job to delete the pdf files
// cron.schedule('*/5 * * * *', () => {
//   console.log('running a task for every five minutes');
//   fs.readdir(directory, (err, files) => {
//     if (err) {
//       console.log(err);
//     };
//     for (const file of files) {

//       fs.stat(directory + file, (err, stat) => {

//         if (err) {
//           return console.error(err);
//         }
//         now = new Date().getTime();
//         endTime = new Date(stat.ctime).getTime() + 600000;
//         // console.log("now------>", now);
//         // console.log("endTime-->", endTime);

//         if (now > endTime) {
//           fs.unlink(path.join(directory, file), err => {
//             if (err) {
//               console.log(err)
//             };
//             console.log("file deleted", file);
//           });
//         }
//       })
//     }
//   });
// });

//cron job to delete pdfDownload files after one hour
// cron.schedule('*/10 * * * *', () => {
//   console.log('running a task for every ten minutes');
//   fs.readdir(pdfDownloadDir, (err, files) => {
//     if (err) {
//       console.log(err);
//     };
//     for (const file of files) {

//       fs.stat(pdfDownloadDir + file, (err, stat) => {

//         if (err) {
//           return console.error(err);
//         }
//         now = new Date().getTime();
//         endTime = new Date(stat.ctime).getTime() + 3600000;
//         // console.log("now------>", now);
//         // console.log("endTime-->", endTime);

//         if (now > endTime) {
//           fs.unlink(path.join(pdfDownloadDir, file), err => {
//             if (err) {
//               console.log(err)
//             };
//             console.log("file deleted", file);
//           });
//         }
//       })
//     }
//   });
// });


// function to get current date 
function getCurrentDate() {
    let dateObj, months, month, day, year, mm, hh, ss, date, time;

    dateObj = new Date();
    months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    month = months[dateObj.getUTCMonth()]; //months from 1-12
    day = dateObj.getUTCDate();
    year = dateObj.getUTCFullYear();
    mm = (dateObj.getMinutes() < 10 ? '0' : '') + dateObj.getMinutes();
    hh = (dateObj.getHours() < 10 ? '0' : '') + dateObj.getHours();
    ss = (dateObj.getSeconds() < 10 ? '0' : '') + dateObj.getSeconds();
    date = day + ' ' + month + ', ' + year;
    time = hh + ":" + mm + ":" + ss;
    return [date, time];
}

// api to send email to cutomer 
app.post('/api/enc/xml-receive', function(req, res) {
    console.log("inside mailsend function----->>");
    let body = "";

    let currentDate = getCurrentDate();
    let dateObj = new Date();
    let month = dateObj.getUTCMonth() + 1; //months from 1-12
    let year = dateObj.getUTCFullYear();
    let newdate = 'bank_statement' + "_" + year + "-" + month;
    let session = new Date().getTime();

    //console.log("Current Date ::", currentDate);
    console.log("newDate ::", newdate);


    // capture the encoded form data
    req.on('data', (chunk) => {

        body = body + chunk;
    })

    req.on('end', () => {
        console.log(new Date().toISOString(), "BEGIN EXEC")
        console.log("END REQ");

        body = body.toString();

        //  console.log("body", body.slice(5));

        // validating json object
        var jsonValidation = IsJsonString(body.slice(5));
        if (jsonValidation == true) {

            var jsonObj = JSON.parse(body.slice(5));
            // return;
            var dataFrmXml = {}
            var customerDetails = {};
            customerDetails.cust1 = {};

            //customer1 details
            customerDetails.cust1.id = jsonObj.data.ReportData.DatasetCustomer1.Customer1Id;
            customerDetails.cust1.firstName = jsonObj.data.ReportData.DatasetCustomer1.Customer1FirstName;
            

                setTimeout(function() {
                    var id = jsonObj.data.ReportData['DatasetCustomer1']['Customer1Id'];
                    var custEmail = jsonObj.data.ReportData['DatasetCustomer1']['Customer1Email'];

                    // using bcrypt to hash the password
                    var hash = bcrypt.hashSync(jsonObj.data.ReportData['DatasetCustomer1']['Customer1Id']);
                    jsonObj.data.ReportData['DatasetCustomer1']['Customer1Password'] = hash;

                    console.log("customerid", customerDetails.cust1.id);

                    // creating an index to store json object
                    client.bulk({
                        body: [
                            // action description
                            {
                                index: {
                                    _index: newdate,
                                    _type: 'bank_statement_logs'
                                }
                            },
                            // the document to index
                            {
                                doc: {
                                    title: jsonObj
                                }
                            },

                        ]
                    }, function(err, resp) {
                        if (err) {
                            res.send({
                                message: err
                            });
                            console.log("DB insertion err", err);
                        }
                        else {
                            console.log("data inserted in db");
                            console.log("response index", resp);
                            // console.log(JSON.stringify(resp.items[0], null, '\t'));
                            var indexName = resp.items[0].index._index;
                            var documentId = resp.items[0].index._id;
                            var customerId = customerDetails.cust1.id;
                            console.log("indexName------------>>", indexName);
                            console.log("documentId------------>>", documentId);
                            // encrypting indexName and documentId
                            const encIndexName = cryptr.encrypt(indexName);
                            const encDocumentId = cryptr.encrypt(documentId);
                            const encCustomerId = cryptr.encrypt(customerId);

                            console.log("url", IP + encIndexName + '_' + encDocumentId);
                            // render dynamic data to html template 
                            var htmlTemplate = fs.readFileSync('views/email.html', 'utf8');
                            htmlTemplate = htmlTemplate.replace(/"{{URL}}"/g, IP + encIndexName + '_' + encDocumentId);
                            htmlTemplate = htmlTemplate.replace(/"{{name}}"/g, customerDetails.cust1.firstName);
                            htmlTemplate = htmlTemplate.replace(/"{{name}}"/g, customerDetails.cust1.firstName);

                            // https://s1.shzy.in/bs/bankLogo.png


                            // email options
                            var email = {
                                to: custEmail,
                                subject: "BankStatement  for customerId ending with " + id.toString().slice(-3),
                                body: '<p>Click <a href="192.168.2.14:2000/bankStatement-data?bankData=' + "url" + '">here</a> to reset your password</p>',
                                html: htmlTemplate
                            };
                            console.log(new Date().toISOString(), "BEGIN EMAIL");

                            // email  sending function
                            send_mail(email.to, email.subject, email.body, email.html, function(err, result1) {

                                // if err
                                if (err) {
                                    console.log("Email error-------->> ", err);
                                    res.send("Email not sent to customer - " + err.response);
                                    // if success
                                }
                                else {
                                    console.log("Email result-------->> ", result1);
                                    console.log("Email sent Successfully");
                                    console.log(new Date().toISOString(), "END EMAIL");
                                    console.log("password for pdf", pdfName, "is", customerId);
                                    res.send("Email sent to customer");

                                }
                                console.log(new Date().toISOString(), "END EXEC");
                            });
                        }
                    });
                }, 1000);
        }
        else if (jsonValidation == false) {
            console.log("Invalid Json");
            res.send("Invalid json")
        }
    });
});



// api to get bank data from db
app.post('/api/enc/bankStatement-data', function(req, res) {
    console.log("inside bankDataGet function........");
    var cipherText = req.body.key;
    let session = new Date().getTime();
    //Decrypt data 
    var bytes = CryptoJS.AES.decrypt(cipherText, 'passwrd');
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

    var encData = decryptedData.cipher.split('_');

    // decrypt indexName and documentId
    var indexName = cryptr.decrypt(encData[0]);
    var documentId = cryptr.decrypt(encData[1]);

    console.log('docId', documentId);
    console.log('indexname', indexName);
    // customerId from body
    var custId = decryptedData.pwd;

    // get data from db based on docId and indexName
    client.search({
        index: indexName,
        type: 'bank_statement_logs',
        body: {
            query: {
                match: {
                    _id: documentId
                }
            }
        }
    }, function(err, resp) {
        if (err) {
            console.log(err);
            res.status(400).send({
                "message": err
            })
        }
        else {

            var data = resp.hits.hits;
            data.forEach(element => {

                //  console.log("data in ==================================================" + JSON.stringify(element._source));
                var customerId = element._source.doc.title.data.ReportData['DatasetCustomer1']['Customer1Id'];

                var dbDataRaw = element._source.doc.title.data;
                var piechartData = dbDataRaw.ReportData['DatasetDoCalculated'];
                //console.log("dataRaw", dbDataRaw.ReportData['DatasettxnData']);
               
                
                var pieChart = [];
                
                for(x in piechartData) {
                    var tempData = {};
                  
                    if(x == "DoCalculatedTotalDebits") {
                        tempData['name'] = "Total Debit";
                        tempData["count"] = Math.abs(piechartData[x].replace(/,/g, ''));
                        pieChart.push(tempData)
                    }
                    if(x == "DoCalculatedTotalCredits") {
                        tempData['name'] = "Total Credits"
                        tempData["count"] = Math.abs(piechartData[x].replace(/,/g, ''));
                        pieChart.push(tempData)
                    }
                   
                }
                dbDataRaw.ReportData.pieChartData = pieChart;
                // res.json(dbDataRaw);
                // return;
  

                // encrypt DB resp
                var encrypted = CryptoJS.AES.encrypt(JSON.stringify(dbDataRaw), 'secret123');
                var dbData = encrypted.toString();

                // comparing password 
                //if matched sending ecrypted bank details 
                if (customerId == custId) {
                    console.log("success");
                    res.status(200).send({
                        status: true,
                        // data:JSON.stringify(element._source.doc.title.data)
                        data: dbData
                    });
                    //if password not matched sending invalid message
                }
                else {
                    res.send({
                        status: false,
                        message: "Invalid Password"
                    });
                }

            });

        }

    });

});


// api to download pdf
app.post('/api/enc/pdf-download', function(req, res) {
    console.log("inside pdfDownload function........");
    // console.log("body", req.body);
    var cipherText = req.body.key;
    let currentDate = getCurrentDate();
    let session = new Date().getTime();

    //Decrypt data 
    var bytes = CryptoJS.AES.decrypt(cipherText, 'passwrd');
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

    var encData = decryptedData.cipher.split('_');

    // decrypt indexName and documentId
    var indexName = cryptr.decrypt(encData[0]);
    var documentId = cryptr.decrypt(encData[1]);

    // customerId from body
    var custId = decryptedData.pwd;

    // get data from db based on docId and indexName
    client.search({
        index: indexName,
        type: 'bank_statement_logs',
        body: {
            query: {
                match: {
                    _id: documentId
                }
            }
        }
    }, function(err, resp) {
        if (err) {
            console.log(err);
            res.status(400).send({
                "message": err
            })
        }
        else {

            var data = resp.hits.hits;
            data.forEach(element => {
                var customerId = element._source.doc.title.data.ReportData['DatasetCustomer1']['Customer1Id'];
                var dbData = element._source.doc.title.data.ReportData;
                var dataFrmXml = {}
                var customerDetails = {};
                customerDetails.cust1 = {};
                var institutionDetails = {};
                var DatasetDoCalculated = {};

                // customerDetails
                customerDetails.name = dbData.DatasetDoInternal.DoInternalCustomerName;
                customerDetails.address1 = dbData.DatasetDoInternal["DoInternalCustomerAddress-1"];
                customerDetails.address2 = dbData.DatasetDoInternal["DoInternalCustomerAddress-2"];
                customerDetails.address3 = dbData.DatasetDoInternal["DoInternalCustomerAddress-3"];
                customerDetails.address4 = dbData.DatasetDoInternal["DoInternalCustomerAddress-4"];
                customerDetails.address5 = dbData.DatasetDoInternal["DoInternalCustomerAddress-5"];
                customerDetails.address6 = dbData.DatasetDoInternal["DoInternalCustomerAddress-6"];

                //institustion Details
                institutionDetails.name = dbData.DatasetDoInternal.DoInternalInstitutionName;
                institutionDetails.address1 = dbData.DatasetDoInternal["DoInternalInstitutionAddress-1"];
                institutionDetails.address2 = dbData.DatasetDoInternal["DoInternalInstitutionAddress-2"];
                institutionDetails.address3 = dbData.DatasetDoInternal["DoInternalInstitutionAddress-3"];
                institutionDetails.address4 = dbData.DatasetDoInternal["DoInternalInstitutionAddress-4"];
                institutionDetails.phoneNum = dbData.DatasetDoInternal["DoInternalInstitutionPhone"];
                institutionDetails.faxNum = dbData.DatasetDoInternal["DoInternalInstitutionFax"];
                institutionDetails.mail = dbData.DatasetDoInternal["DoInternalInstitutionEmail"];

                //footer details
                dataFrmXml.instFooter1 = dbData.DatasetDoInternal["DoInternalLegalFooter-1"];
                dataFrmXml.instFooter2 = dbData.DatasetDoInternal["DoInternalLegalFooter-2"];
                dataFrmXml.instFooter3 = dbData.DatasetDoInternal["DoInternalLegalFooter-3"];
                dataFrmXml.instFooter4 = dbData.DatasetDoInternal["DoInternalLegalFooter-4"];

                //bank balance details
                DatasetDoCalculated.OpeningBalance = dbData.DatasetDoCalculated.DoCalculatedOpeningBalance;
                DatasetDoCalculated.TotalDebits = dbData.DatasetDoCalculated.DoCalculatedTotalDebits;
                DatasetDoCalculated.TotalCredits = dbData.DatasetDoCalculated.DoCalculatedTotalCredits;
                DatasetDoCalculated.ActualBalance = dbData.DatasetDoCalculated.DoCalculatedActualBalance;
                DatasetDoCalculated.ClearedBalance = dbData.DatasetDoCalculated.DoCalculatedClearedBalance;

                //customer1 details
                customerDetails.cust1.id = dbData.DatasetCustomer1.Customer1Id;
                customerDetails.cust1.firstName = dbData.DatasetCustomer1.Customer1FirstName;
                customerDetails.cust1.lastName = dbData.DatasetCustomer1.Customer1LastName;
                customerDetails.cust1.address = dbData.DatasetCustomer1.Customer1Address;
                customerDetails.cust1.title = dbData.DatasetCustomer1.Customer1Title;
                customerDetails.cust1.email = dbData.DatasetCustomer1.Customer1Email;
                customerDetails.cust1.bankLogo = dbData.DatasetCustomer1.Customer1BankLogo;
                customerDetails.cust1.productCode = dbData.DatasetCustomer1.Customer1ProductCode;
                customerDetails.cust1.product = dbData.DatasetCustomer1.Customer1Product;
                customerDetails.cust1.branch = dbData.DatasetCustomer1.Customer1Branch;
                customerDetails.cust1.accountNo = dbData.DatasetCustomer1.Customer1AccountNo;
                customerDetails.cust1.currency = dbData.DatasetCustomer1.Customer1Currency;
                customerDetails.cust1.stmtNo = dbData.DatasetCustomer1.Customer1StmtNo;
                customerDetails.cust1.startDate = dbData.DatasetCustomer1.Customer1StartDate;
                customerDetails.cust1.endDate = dbData.DatasetCustomer1.Customer1EndDate;
                customerDetails.cust1.accountOpendate = dbData.DatasetCustomer1.Customer1AcctOpenDate;

                var row = dbData.DatasetStatement['Row'];
                var dataAll = [];
                var totalcount = 0;
                var data = [];
                row.forEach(element => {
                    // console.log("count", element.StatementNarrative.toString());
                    var count = 0
                    count = element.StatementNarrative.length;
                    //console.log("count", count);
                    totalcount = totalcount + count;
                    // console.log("totalcount1", totalcount);
                    if (totalcount <= 15) {
                        data.push(element);
                    }
                    else {
                        console.log("totalcount", totalcount);
                        totalcount = 0;
                        dataAll.push(data);
                        data = [];
                    }
                });

                let ejs = require('ejs');


                var frmDt = customerDetails.cust1.startDate.split(' ').join('_');
                var toDt = customerDetails.cust1.endDate.split(' ').join('_');
                var accNo = customerDetails.cust1.accountNo;
                var stateNo = customerDetails.cust1.stmtNo;

                console.log("frmDt", frmDt, "toDt", toDt);

                var pdfName = accNo + '_' + frmDt + '-' + toDt + '_' + stateNo;

                console.log(new Date().toISOString(), "BEGIN PDF");
                
                //local
                //var pdfPath = pdfDownloadDir;
                
                //server
                var pdfPath = '/opt/ss/smart-statement/public/pdfDownload/';
                
                var pdfPathArr = [];
                let summaryHtml = ejs.render(summaryHtmlTemplate, {
                    // data: file,
                    customer: customerDetails,
                    customer1: customerDetails.cust1,
                    instDetails: institutionDetails,
                    footer: dataFrmXml,
                    dataset: DatasetDoCalculated,
                    bankLogo: bankLogo,
                    currentDate: currentDate,
                    totalPages: dataAll.length + 1,
                    pageCount: 1
                });
                pdf.create(summaryHtml, options).toFile(pdfPath + 'summary_' + pdfName + '.pdf', function(err, res2) {
                    if (err) {
                        console.log(err)
                    }
                    console.log('summary ', res2.filename)
                    pdfPathArr.push(res2.filename);

                    var pdfCount = 1
                    async.eachLimit(dataAll, 1, function(file, callback) {
                        // console.log("file --->>", file);
                        var totalCDdata = run(file);
                       //var totalCDdata = {"openingBalance":"1234","closingBalance":"1234","pagetotalCredit":1234,"pagetotalDebit":1234}
                        //console.log("totalCDdata", totalCDdata);
                        
                        pdfCount = pdfCount + 1;
                        let sampleHtml = ejs.render(sampleHtmlTemplate, {
                            data: file,
                            customer: customerDetails,
                            customer1: customerDetails.cust1,
                            instDetails: institutionDetails,
                            footer: dataFrmXml,
                            dataset: DatasetDoCalculated,
                            bankLogo: bankLogo,
                            currentDate: currentDate,
                            totalPages: dataAll.length + 1,
                            pageCount: pdfCount,
                            totalCDdata: totalCDdata,
                        });

                        console.log("dataAll", dataAll.length + 1);
                        console.log("pageCount", pdfCount);
                        // var pathPdf = path.join(__dirname + '/pdf/encrypt' + pdfCount + '.pdf');

                        pdf.create(sampleHtml, options).toFile(pdfPath + pdfCount + 'page_' + pdfName + '.pdf', function(err, res1) {
                            console.log('PDF', err, res1);
                            pdfPathArr.push(res1.filename);
                            callback(err, pdfPathArr);
                        });
                    }, function(err, paths) {
                        // if any of the file processing produced an error, err would equal that error
                        if (err) {
                            console.log(err)
                            console.log('A file failed to process');
                        }
                        else {
                            var finaldata = [];
                            finaldata.push(pdfPath + 'summary_' + pdfName + '.pdf');
                            for (var i = 2; i <= pdfCount; i++) {
                                finaldata.push(pdfPath + i + 'page_' + pdfName + '.pdf');
                            }

                            console.log('(pdfPath final', finaldata)
                            merge(finaldata, pdfPath + session + 'encrypt.pdf', function(err) {
                                if (err) {
                                    return console.log("err", err)
                                }
                                console.log('Successfully merged!');
                                const pdfDoc = new HummusRecipe(pdfPath + session + 'encrypt.pdf', pdfPath + session + '_' + pdfName + '.pdf');

                                // pdf encryption with customerId as password
                                pdfDoc
                                    .encrypt({
                                        userPassword: custId.toString(),
                                        ownerPassword: custId.toString(),
                                        userProtectionFlag: 4
                                    })
                                    .endPDF();
                                console.log(new Date().toISOString(), "END PDF");
                                console.log("Download file ", 'http://' + req.get('host') + '/tempfile/' + session + '_' + pdfName + '.pdf')
                                res.status(200).send({
                                    status: true,
                                    fileName: 'http://' + req.get('host') + '/tempfile/' + session + '_' + pdfName + '.pdf'
                                });
                            });
                            // console.log('All files have been processed successfully');
                        }
                    });
                });

            });

        }

    });

});

function run(context) {
      var result = {};
      var totalCredit = 0;
      var totalDebit = 0;
      for (var i = 0; i < context.length; i++) {
          var data = context[i];
          if (i == 0) result.openingBalance = data.StatementRunningBalance;
          if (i == (context.length - 1)) result.closingBalance = data.StatementRunningBalance;
          totalDebit += Number(data.StatementWithdrawal.replace(/,/g, ""));
          totalCredit += Number(data.StatementLodgement.replace(/,/g, ""));
      }
      result.pagetotalCredit = totalCredit;
      result.pagetotalDebit = totalDebit;
      return result;
  }

app.post('/data-sort', function(req, res) {
    console.log("/data-sort")
    var id = "3BJhWXEBrYw03eMyHqS4";
    client.search({
        index: "bank_statement_2020-4",
        type: 'bank_statement_logs',
        body: {
            query: {
                match: {
                    _id: id
                }
            },
            sort: [{
                "doc.titledata.ReportData.DatasetDoInternal.DoInternalCustomerName": {
                    order: "asc"
                    // nested: {
                    //     path: "doc['title']['data']['ReportData']['DatasetStatement']['Row']"
                    // }
                }
            }]
            //       sort : [
            //       {
            //          "doc['title']['data']['ReportData']['DatasetStatement']['Row']['StatementBookingDate']" : {
            //             "order" : "asc",
            //             "nested_filter":{match: {
            //           _id: id
            //         }},
            //             // "nested_path" : "Row"
            //          }
            //       }
            //   ],

        }
    }, function(err, resp) {
        if (err) {
            console.log(err);
            res.status(400).send({
                "message": err
            })
        }
        else {

            var data = resp.hits.hits;
            data.forEach(element => {

                //  console.log("data in ==================================================" + JSON.stringify(element._source));
                var customerId = element._source.doc.title.data.ReportData['DatasetCustomer1']['Customer1Id'];


                res.json({
                    status: true,
                    data: element._source.doc.title.data
                })


            });

        }

    });
})

//Listen port
let port = 8013;


//application starting
var server = app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});

//server.setTimeout(5000000)
