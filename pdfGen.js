//importing libraries
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
const fs = require('fs');
const shell = require('shelljs');
const HummusRecipe = require('hummus-recipe');
const pdf = require('html-pdf');
var myArgs = process.argv.slice(2);
//console.log('myArgs: ', myArgs);
let session = new Date().getMilliseconds();
fs.readFile(myArgs[0], function(err, data) {
    //res.writeHead(200, {'Content-Type': 'text/html'});
    // res.write(data);
    // res.end();
     
    var options = {

        "type": "pdf",
        "format": "A4",

        "header": {
          "height": '10mm'
        },
        "footer": {
          "height": "10mm",
        },
        "orientation": "portrait",
        "zoomFactor": "1",
        "border": {
          "top": '0',
          "right": "3mm",
          "left": "5mm",
          "bottom": "1mm"
        },
        "timeout": 600000
      }

  
      // html to pdf conversion
      pdf.create(""+data, options).toFile('pdf/' + session + 'encrypt.pdf', function (err, res1) {
        if (err) {
          console.log("pdf err", err);
        }

        const pdfDoc = new HummusRecipe('pdf/' + session + 'encrypt.pdf', myArgs[1]);

        // pdf encryption with customerId as password
        pdfDoc
          .encrypt({
            userPassword: myArgs[2],
            ownerPassword: myArgs[2],
            userProtectionFlag: 4
          })
          .endPDF();
        console.log(new Date().toISOString(), "END PDF")
        });
    });

   