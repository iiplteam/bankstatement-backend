var request = require("request");

const fs = require('fs');


var options = { method: 'POST',
  url: 'http://localhost/api/enc/xml-receive',
  headers: 
   { 'postman-token': '7c2f15b1-96ca-41b7-5a95-519cebb1f7c5',
     'cache-control': 'no-cache',
     'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
  formData: { data: JSON.stringify(fs.readFileSync('test.json')) }
 };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});

