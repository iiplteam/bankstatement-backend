//importing libraries
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
const fs = require('fs')
const HummusRecipe = require('hummus-recipe');
const path = require('path');
const bcrypt = require('bcrypt-nodejs')
var CryptoJS = require('crypto-js');
var htmlPdfTemplate = fs.readFileSync(path.join(__dirname, 'views/pdf.ejs'), 'utf8');
var sampleHtmlTemplate = fs.readFileSync(path.join(__dirname, 'views/statement.ejs'), 'utf8');
//const pdf = require('html-pdf');
var currencyFormatter = require('currency-formatter');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
var cron = require('node-cron');
const directory = '/home/t24smart/pdf/';
const shell = require('shelljs');
const pdf = require('html-pdf');
var nodemailer = require('nodemailer');
const pdfDownloadDir = '/opt/ss/smart-statement/public/pdfDownload/';

// importing nodemailer
var nodemailer = require('nodemailer');
var ip = require("ip");


// from address for mail
//var fromAdress = "Bank of Abyssinia Statement <smartstatement@bankofabyssinia.com>";
var fromAdress = "Smart Statement Bank of Abyssinia<smart-statement@wrkwth.com>"

var options = {

  "type": "pdf",
  "format": "A4",

  "header": {
    "height": '10mm'
  },
  "footer": {
    "height": "10mm",
  },
  "orientation": "portrait",
  "zoomFactor": "1",
  "border": {
    "top": '0',
    "right": "3mm",
    "left": "5mm",
    "bottom": "1mm"
  },
  "timeout": 600000
};

// nodemailer SMTP 
// var options = {
//     host: "mail.bankofabyssinia.com",
//     port: 465,
//     //ignoreTLS: true,
//     secure: true, // use TLS
//     auth: {
//       user: "smartstatement@bankofabyssinia.com",
//       pass: "*AAaa123123*@@"
//     },
//     // tls: {
//     //   // do not fail on invalid certs
//     //   rejectUnauthorized: false
//     // }
//   }
//   let smtpTransport = nodemailer.createTransport(options)

//smtp configuration using aws
var smtpTransport = nodemailer.createTransport({
  host: "email-smtp.us-east-1.amazonaws.com",
  port: 465,
  secure: true,
  auth: {
    user: "AKIAIAJSIT2B3VM6ML2A",
    pass: "BP3Pb7s5148Q3rHJ2jbLCR5sZzGq2/T7yTEyRQ9WKkvS"
  },
  tls: {
    rejectUnauthorized: false
  }
});

// function to send mail
function send_mail(to, subject, body, html, attachments, size, cb) {
  var obj = {
    from: fromAdress, // sender address
    to: to, // list of receivers
    subject: subject, // Subject line
    text: body, // plain text body
    html: html // html body
  };

  if (size < 9) {
    obj.attachments = attachments;
  }

  smtpTransport.sendMail(
    obj
    , function (err, data) {

      cb(err, data);
    });
};
// importing elastic search
const { Client } = require('elasticsearch');

const client = new Client({
  host: 'localhost:9200',
  requestTimeout: 60000
  //log: 'trace'
});

// client ping
client.ping({
  requestTimeout: 3000
}, function (error) {
  if (error) {
    console.trace('Error:', error);
  } else {
    console.log('Elasticsearch Connected!');
  }
  // on finish
  //client.close();
});

//banklogo

const bankLogo = "http://s1.shzy.in/bs/bankLogo.png"
const IP = "http://196.188.92.168/bankStatement/Id/"
//const IP = "http://192.168.1.107:8013/bankStatement/Id/"

//running the express 
var app = express();

//Integrating cors and bodyParser plugins to express
app.use(cors());
app.use(bodyParser.json())


// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(express.static('public'));

app.get('/test', function (req, res) {
  console.log("WORKING...");
  res.send("WORKING...")
});

app.get('/tempfile/:id', function (req, res) {
  let id = req.params.id;
  // console.log("server.address() ", req.get('host'));
  // console.log("tempfile", id);
  // res.download("/home/t24smart/7146515_01_AUG_2019-06_AUG_2019_39.pdf");
  res.download(__dirname + '/public/pdfDownload/' + id);
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/public/index.html'));
});



// function to validate json 
function IsJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

// function to get pdf file size
function getFileSize(filePath) {
  var stats = fs.statSync(filePath);
  var size = stats["size"];
  var fileSizeInMegabytes = size / 1000000.0
  return fileSizeInMegabytes;
}

// cron job to delete the pdf files
cron.schedule('*/5 * * * *', () => {
  console.log('running a task for every five minutes');
  fs.readdir(directory, (err, files) => {
    if (err) {
      console.log(err);
    };
    for (const file of files) {

      fs.stat('/home/t24smart/pdf/' + file, (err, stat) => {

        if (err) {
          return console.error(err);
        }
        now = new Date().getTime();
        endTime = new Date(stat.ctime).getTime() + 600000;
        // console.log("now------>", now);
        // console.log("endTime-->", endTime);

        if (now > endTime) {
          fs.unlink(path.join(directory, file), err => {
            if (err) {
              console.log(err)
            };
            console.log("file deleted", file);
          });
        }
      })
    }
  });
});

// cron job to delete pdfDownload files after one hour
cron.schedule('*/10 * * * *', () => {
  console.log('running a task for every ten minutes');
  fs.readdir(pdfDownloadDir, (err, files) => {
    if (err) {
      console.log(err);
    };
    for (const file of files) {

      fs.stat(pdfDownloadDir + file, (err, stat) => {

        if (err) {
          return console.error(err);
        }
        now = new Date().getTime();
        endTime = new Date(stat.ctime).getTime() + 3600000;
        // console.log("now------>", now);
        // console.log("endTime-->", endTime);

        if (now > endTime) {
          fs.unlink(path.join(pdfDownloadDir, file), err => {
            if (err) {
              console.log(err)
            };
            console.log("file deleted", file);
          });
        }
      })
    }
  });
});


// function to get current date 
function getCurrentDate() {
  let dateObj, months, month, day, year, mm, hh, ss, date, time;

  dateObj = new Date();
  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  month = months[dateObj.getUTCMonth()]; //months from 1-12
  day = dateObj.getUTCDate();
  year = dateObj.getUTCFullYear();
  mm = (dateObj.getMinutes() < 10 ? '0' : '') + dateObj.getMinutes();
  hh = (dateObj.getHours() < 10 ? '0' : '') + dateObj.getHours();
  ss = (dateObj.getSeconds() < 10 ? '0' : '') + dateObj.getSeconds();
  date = day + ' ' + month + ', ' + year;
  time = hh + ":" + mm + ":" + ss;
  return [date, time];
}

// api to send email to cutomer 
app.post('/api/enc/xml-receive', function (req, res) {
  console.log("inside mailsend function----->>");
  let body = "";

  let currentDate = getCurrentDate();
  let dateObj = new Date();
  let month = dateObj.getUTCMonth() + 1; //months from 1-12
  let year = dateObj.getUTCFullYear();
  let newdate = 'bank_statement' + "_" + year + "-" + month;
  let session = new Date().getTime();

  //console.log("Current Date ::", currentDate);
  //console.log("newDate ::", newdate);


  // capture the encoded form data
  req.on('data', (chunk) => {

    body = body + chunk;
  })

  req.on('end', () => {
    console.log(new Date().toISOString(), "BEGIN EXEC")
    console.log("END REQ");

    body = body.toString();

    //  console.log("body", body.slice(5));

    // validating json object
    var jsonValidation = IsJsonString(body.slice(5));
    if (jsonValidation == true) {

      var jsonObj = JSON.parse(body.slice(5));
      // return;
      var dataFrmXml = {}
      var customerDetails = {};
      customerDetails.cust1 = {};
      var institutionDetails = {};
      var DatasetDoCalculated = {};

      // customerDetails
      customerDetails.name = jsonObj.data.ReportData.DatasetDoInternal.DoInternalCustomerName;
      customerDetails.address1 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalCustomerAddress-1"];
      customerDetails.address2 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalCustomerAddress-2"];
      customerDetails.address3 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalCustomerAddress-3"];
      customerDetails.address4 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalCustomerAddress-4"];
      customerDetails.address5 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalCustomerAddress-5"];
      customerDetails.address6 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalCustomerAddress-6"];

      //institustion Details
      institutionDetails.name = jsonObj.data.ReportData.DatasetDoInternal.DoInternalInstitutionName;
      institutionDetails.address1 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalInstitutionAddress-1"];
      institutionDetails.address2 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalInstitutionAddress-2"];
      institutionDetails.address3 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalInstitutionAddress-3"];
      institutionDetails.address4 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalInstitutionAddress-4"];
      institutionDetails.phoneNum = jsonObj.data.ReportData.DatasetDoInternal["DoInternalInstitutionPhone"];
      institutionDetails.faxNum = jsonObj.data.ReportData.DatasetDoInternal["DoInternalInstitutionFax"];
      institutionDetails.mail = jsonObj.data.ReportData.DatasetDoInternal["DoInternalInstitutionEmail"];

      //footer details
      dataFrmXml.instFooter1 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalLegalFooter-1"];
      dataFrmXml.instFooter2 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalLegalFooter-2"];
      dataFrmXml.instFooter3 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalLegalFooter-3"];
      dataFrmXml.instFooter4 = jsonObj.data.ReportData.DatasetDoInternal["DoInternalLegalFooter-4"];

      //bank balance details
      DatasetDoCalculated.OpeningBalance = jsonObj.data.ReportData.DatasetDoCalculated.DoCalculatedOpeningBalance;
      DatasetDoCalculated.TotalDebits = jsonObj.data.ReportData.DatasetDoCalculated.DoCalculatedTotalDebits;
      DatasetDoCalculated.TotalCredits = jsonObj.data.ReportData.DatasetDoCalculated.DoCalculatedTotalCredits;
      DatasetDoCalculated.ActualBalance = jsonObj.data.ReportData.DatasetDoCalculated.DoCalculatedActualBalance;
      DatasetDoCalculated.ClearedBalance = jsonObj.data.ReportData.DatasetDoCalculated.DoCalculatedClearedBalance;

      //customer1 details
      customerDetails.cust1.id = jsonObj.data.ReportData.DatasetCustomer1.Customer1Id;
      customerDetails.cust1.firstName = jsonObj.data.ReportData.DatasetCustomer1.Customer1FirstName;
      customerDetails.cust1.lastName = jsonObj.data.ReportData.DatasetCustomer1.Customer1LastName;
      customerDetails.cust1.address = jsonObj.data.ReportData.DatasetCustomer1.Customer1Address;
      customerDetails.cust1.title = jsonObj.data.ReportData.DatasetCustomer1.Customer1Title;
      customerDetails.cust1.email = jsonObj.data.ReportData.DatasetCustomer1.Customer1Email;
      customerDetails.cust1.bankLogo = jsonObj.data.ReportData.DatasetCustomer1.Customer1BankLogo;
      customerDetails.cust1.productCode = jsonObj.data.ReportData.DatasetCustomer1.Customer1ProductCode;
      customerDetails.cust1.product = jsonObj.data.ReportData.DatasetCustomer1.Customer1Product;
      customerDetails.cust1.branch = jsonObj.data.ReportData.DatasetCustomer1.Customer1Branch;
      customerDetails.cust1.accountNo = jsonObj.data.ReportData.DatasetCustomer1.Customer1AccountNo;
      customerDetails.cust1.currency = jsonObj.data.ReportData.DatasetCustomer1.Customer1Currency;
      customerDetails.cust1.stmtNo = jsonObj.data.ReportData.DatasetCustomer1.Customer1StmtNo;
      customerDetails.cust1.startDate = jsonObj.data.ReportData.DatasetCustomer1.Customer1StartDate;
      customerDetails.cust1.endDate = jsonObj.data.ReportData.DatasetCustomer1.Customer1EndDate;

      var row = jsonObj.data.ReportData.DatasetStatement['Row'];

      //console.log("customerdetails", customerDetails)
      var custId = customerDetails.cust1.id;
      console.log("email", customerDetails.cust1.email);
      // console.log(custId.toString(), typeof(custId));

      row.forEach(element => {

        var arr = []

        if (element.StatementNarrative.indexOf('<br>') !== -1) {
          let a = element.StatementNarrative.split('<br>');
          arr = a;
        } else {
          arr.push(element.StatementNarrative);
        }
        element.StatementNarrative = arr;
      });

      row.forEach(element => {

        // format currency
        element.StatementRunningBalance = currencyFormatter.format(element.StatementRunningBalance, {
          code: ''
        });
        element.StatementLodgement = currencyFormatter.format(element.StatementLodgement, {
          code: ''
        });
        element.StatementWithdrawal = currencyFormatter.format(element.StatementWithdrawal, {
          code: ''
        });
      });

      // format currency
      DatasetDoCalculated.OpeningBalance = currencyFormatter.format(DatasetDoCalculated.OpeningBalance, {
        code: ''
      });
      DatasetDoCalculated.TotalDebits = currencyFormatter.format(DatasetDoCalculated.TotalDebits, {
        code: ''
      });
      DatasetDoCalculated.TotalCredits = currencyFormatter.format(DatasetDoCalculated.TotalCredits, {
        code: ''
      });
      DatasetDoCalculated.ActualBalance = currencyFormatter.format(DatasetDoCalculated.ActualBalance, {
        code: ''
      });
      DatasetDoCalculated.ClearedBalance = currencyFormatter.format(DatasetDoCalculated.ClearedBalance, {
        code: ''
      });

      //console.log(DatasetDoCalculated);

      // rendering dynamic data to pdf
      let ejs = require('ejs'),
        html = ejs.render(htmlPdfTemplate, {
          data: row,
          customer: customerDetails,
          customer1: customerDetails.cust1,
          instDetails: institutionDetails,
          footer: dataFrmXml,
          dataset: DatasetDoCalculated,
          bankLogo: bankLogo
        },
          sampleHtml = ejs.render(sampleHtmlTemplate, {
            data: row,
            customer: customerDetails,
            customer1: customerDetails.cust1,
            instDetails: institutionDetails,
            footer: dataFrmXml,
            dataset: DatasetDoCalculated,
            bankLogo: bankLogo,
            currentDate: currentDate
          }));

      //console.log(sampleHtml);
      var pdfPath = '/home/t24smart/pdf/' + session + 'BankStatement.pdf'
      //var shellArgs = 'node pdfGen.js html/sample.html ' +pdfPath+' ' +custId.toString();
      //console.log("shellArgs", shellArgs);

      /*          fs.writeFile('html/sample.html', sampleHtml, function (err,data) {
                  if (err) 
                  {
      console.log("fs err",err.message);
                  }else{
                    console.log('Saved!',data);
                    if (shell.exec(shellArgs).code !== 0) {
                      shell.echo('Error: Git commit failed');
                      shell.exit(1);
                    }
                  }
                  
                });
      */
      console.log(new Date().toISOString(), "BEGIN PDF");
      pdf.create(sampleHtml, options).toFile('/home/t24smart/pdf/' + session + 'encrypt.pdf', function (err, res1) {
        console.log('PDF', err, res1);

        if (err) {
          console.log("pdf err", err);
        }
        var fileSize = getFileSize('/home/t24smart/pdf/' + session + 'encrypt.pdf');
        console.log('PDF FILE SIZE------------->>', fileSize + ' MB')
        const pdfDoc = new HummusRecipe('/home/t24smart/pdf/' + session + 'encrypt.pdf', pdfPath);

        // pdf encryption with customerId as password
        pdfDoc
          .encrypt({
            userPassword: custId.toString(),
            ownerPassword: custId.toString(),
            userProtectionFlag: 4
          })
          .endPDF();
        console.log(new Date().toISOString(), "END PDF");

        var frmDt = customerDetails.cust1.startDate.split(' ').join('_');
        var toDt = customerDetails.cust1.endDate.split(' ').join('_');
        var accNo = customerDetails.cust1.accountNo;
        var stateNo = customerDetails.cust1.stmtNo;

        //  console.log("frmDt", frmDt, "toDt", toDt );

        var pdfName = accNo + '_' + frmDt + '-' + toDt + '_' + stateNo;


        setTimeout(function () {
          var attachments = [{
            filename: pdfName + '.pdf',
            path: pdfPath, // stream this file
            encoding: 'base64'
          }]


          var id = jsonObj.data.ReportData['DatasetCustomer1']['Customer1Id'];
          var custEmail = jsonObj.data.ReportData['DatasetCustomer1']['Customer1Email'];

          // using bcrypt to hash the password
          var hash = bcrypt.hashSync(jsonObj.data.ReportData['DatasetCustomer1']['Customer1Id']);
          jsonObj.data.ReportData['DatasetCustomer1']['Customer1Password'] = hash;

          console.log("customerid", customerDetails.cust1.id);

          // creating an index to store json object
          client.bulk({
            body: [
              // action description
              {
                index: {
                  _index: newdate,
                  _type: 'bank_statement_logs'
                }
              },
              // the document to index
              {
                doc: {
                  title: jsonObj
                }
              },

            ]
          }, function (err, resp) {
            if (err) {
              res.send({
                message: err
              });
              console.log("DB insertion err", err);
            } else {
              console.log("data inserted in db");
              // console.log("response index", resp);
              // console.log(JSON.stringify(resp.items[0], null, '\t'));
              var indexName = resp.items[0].index._index;
              var documentId = resp.items[0].index._id;
              var customerId = customerDetails.cust1.id;

              // encrypting indexName and documentId
              const encIndexName = cryptr.encrypt(indexName);
              const encDocumentId = cryptr.encrypt(documentId);
              const encCustomerId = cryptr.encrypt(customerId);


              // render dynamic data to html template 
              var htmlTemplate = fs.readFileSync('views/email.html', 'utf8');
              htmlTemplate = htmlTemplate.replace(/"{{URL}}"/g, IP + encIndexName + '_' + encDocumentId);
              htmlTemplate = htmlTemplate.replace(/"{{name}}"/g, customerDetails.cust1.firstName);
              htmlTemplate = htmlTemplate.replace(/"{{name}}"/g, customerDetails.cust1.firstName);

              // https://s1.shzy.in/bs/bankLogo.png


              // email options
              var email = {
                to: custEmail,
                subject: "BankStatement  for customerId ending with " + id.toString().slice(-3),
                body: '<p>Click <a href="192.168.2.14:2000/bankStatement-data?bankData=' + "url" + '">here</a> to reset your password</p>',
                html: htmlTemplate,
                attachments: attachments

              };
              console.log(new Date().toISOString(), "BEGIN EMAIL");

              // email  sending function
              send_mail(email.to, email.subject, email.body, email.html, email.attachments, fileSize, function (err, result1) {

                // if err
                if (err) {
                  console.log("Email error-------->> ", err);
                  res.send("Email not sent to customer - " + err.response);
                  // if success
                } else {
                  console.log("Email result-------->> ", result1);
                  console.log("Email sent Successfully");
                  console.log(new Date().toISOString(), "END EMAIL");
                  console.log("password for pdf", pdfName, "is", customerId);
                  res.send("Email sent to customer");

                }
                console.log(new Date().toISOString(), "END EXEC");
              });
            }
          });
        }, 1000);
      });
    } else if (jsonValidation == false) {
      console.log("Invalid Json");
      res.send("Invalid json")
    }
  });
});

// api to get bank data from db
app.post('/api/enc/bankStatement-data', function (req, res) {
  console.log("inside bankDataGet function........");
  var cipherText = req.body.key;
  let session = new Date().getTime();
  //Decrypt data 
  var bytes = CryptoJS.AES.decrypt(cipherText, 'passwrd');
  var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

  var encData = decryptedData.cipher.split('_');

  // decrypt indexName and documentId
  var indexName = cryptr.decrypt(encData[0]);
  var documentId = cryptr.decrypt(encData[1]);
  
  console.log('docId', documentId);
  console.log('indexname', indexName);
  // customerId from body
  var custId = decryptedData.pwd;

  // get data from db based on docId and indexName
  client.search({
    index: indexName,
    type: 'bank_statement_logs',
    body: {
      query: {
        match: {
          _id: documentId
        }
      }
    }
  }, function (err, resp) {
    if (err) {
      console.log(err);
      res.status(400).send({
        "message": err
      })
    } else {

      var data = resp.hits.hits;
      data.forEach(element => {

        //   console.log("data in ==================================================" + JSON.stringify(element._source));
        var customerId = element._source.doc.title.data.ReportData['DatasetCustomer1']['Customer1Id'];

        // encrypt DB resp
        var encrypted = CryptoJS.AES.encrypt(JSON.stringify(element._source.doc.title.data), 'secret123');
        var dbData = encrypted.toString();

        // comparing password 
        //if matched sending ecrypted bank details 
        if (customerId == custId) {
          console.log("success");
          res.status(200).send({
            status: true,
            data: dbData
          });
          //if password not matched sending invalid message
        } else {
          res.send({
            status: false,
            message: "Invalid Password"
          });
        }

      });

    }

  });

});


// api to download pdf
app.post('/api/enc/pdf-download', function (req, res) {
  console.log("inside pdfDownload function........");
  // console.log("body", req.body);
  var cipherText = req.body.key;
  let currentDate = getCurrentDate();
  let session = new Date().getTime();

  //Decrypt data 
  var bytes = CryptoJS.AES.decrypt(cipherText, 'passwrd');
  var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

  var encData = decryptedData.cipher.split('_');

  // decrypt indexName and documentId
  var indexName = cryptr.decrypt(encData[0]);
  var documentId = cryptr.decrypt(encData[1]);

  // customerId from body
  var custId = decryptedData.pwd;

  // get data from db based on docId and indexName
  client.search({
    index: indexName,
    type: 'bank_statement_logs',
    body: {
      query: {
        match: {
          _id: documentId
        }
      }
    }
  }, function (err, resp) {
    if (err) {
      console.log(err);
      res.status(400).send({
        "message": err
      })
    } else {

      var data = resp.hits.hits;
      data.forEach(element => {


        var customerId = element._source.doc.title.data.ReportData['DatasetCustomer1']['Customer1Id'];
        var dbData = element._source.doc.title.data.ReportData;
        var dataFrmXml = {}
        var customerDetails = {};
        customerDetails.cust1 = {};
        var institutionDetails = {};
        var DatasetDoCalculated = {};

        // customerDetails
        customerDetails.name = dbData.DatasetDoInternal.DoInternalCustomerName;
        customerDetails.address1 = dbData.DatasetDoInternal["DoInternalCustomerAddress-1"];
        customerDetails.address2 = dbData.DatasetDoInternal["DoInternalCustomerAddress-2"];
        customerDetails.address3 = dbData.DatasetDoInternal["DoInternalCustomerAddress-3"];
        customerDetails.address4 = dbData.DatasetDoInternal["DoInternalCustomerAddress-4"];
        customerDetails.address5 = dbData.DatasetDoInternal["DoInternalCustomerAddress-5"];
        customerDetails.address6 = dbData.DatasetDoInternal["DoInternalCustomerAddress-6"];

        //institustion Details
        institutionDetails.name = dbData.DatasetDoInternal.DoInternalInstitutionName;
        institutionDetails.address1 = dbData.DatasetDoInternal["DoInternalInstitutionAddress-1"];
        institutionDetails.address2 = dbData.DatasetDoInternal["DoInternalInstitutionAddress-2"];
        institutionDetails.address3 = dbData.DatasetDoInternal["DoInternalInstitutionAddress-3"];
        institutionDetails.address4 = dbData.DatasetDoInternal["DoInternalInstitutionAddress-4"];
        institutionDetails.phoneNum = dbData.DatasetDoInternal["DoInternalInstitutionPhone"];
        institutionDetails.faxNum = dbData.DatasetDoInternal["DoInternalInstitutionFax"];
        institutionDetails.mail = dbData.DatasetDoInternal["DoInternalInstitutionEmail"];

        //footer details
        dataFrmXml.instFooter1 = dbData.DatasetDoInternal["DoInternalLegalFooter-1"];
        dataFrmXml.instFooter2 = dbData.DatasetDoInternal["DoInternalLegalFooter-2"];
        dataFrmXml.instFooter3 = dbData.DatasetDoInternal["DoInternalLegalFooter-3"];
        dataFrmXml.instFooter4 = dbData.DatasetDoInternal["DoInternalLegalFooter-4"];

        //bank balance details
        DatasetDoCalculated.OpeningBalance = dbData.DatasetDoCalculated.DoCalculatedOpeningBalance;
        DatasetDoCalculated.TotalDebits = dbData.DatasetDoCalculated.DoCalculatedTotalDebits;
        DatasetDoCalculated.TotalCredits = dbData.DatasetDoCalculated.DoCalculatedTotalCredits;
        DatasetDoCalculated.ActualBalance = dbData.DatasetDoCalculated.DoCalculatedActualBalance;
        DatasetDoCalculated.ClearedBalance = dbData.DatasetDoCalculated.DoCalculatedClearedBalance;

        //customer1 details
        customerDetails.cust1.id = dbData.DatasetCustomer1.Customer1Id;
        customerDetails.cust1.firstName = dbData.DatasetCustomer1.Customer1FirstName;
        customerDetails.cust1.lastName = dbData.DatasetCustomer1.Customer1LastName;
        customerDetails.cust1.address = dbData.DatasetCustomer1.Customer1Address;
        customerDetails.cust1.title = dbData.DatasetCustomer1.Customer1Title;
        customerDetails.cust1.email = dbData.DatasetCustomer1.Customer1Email;
        customerDetails.cust1.bankLogo = dbData.DatasetCustomer1.Customer1BankLogo;
        customerDetails.cust1.productCode = dbData.DatasetCustomer1.Customer1ProductCode;
        customerDetails.cust1.product = dbData.DatasetCustomer1.Customer1Product;
        customerDetails.cust1.branch = dbData.DatasetCustomer1.Customer1Branch;
        customerDetails.cust1.accountNo = dbData.DatasetCustomer1.Customer1AccountNo;
        customerDetails.cust1.currency = dbData.DatasetCustomer1.Customer1Currency;
        customerDetails.cust1.stmtNo = dbData.DatasetCustomer1.Customer1StmtNo;
        customerDetails.cust1.startDate = dbData.DatasetCustomer1.Customer1StartDate;
        customerDetails.cust1.endDate = dbData.DatasetCustomer1.Customer1EndDate;

        var row = dbData.DatasetStatement['Row'];
        let ejs = require('ejs'),
          sampleHtml = ejs.render(sampleHtmlTemplate, {
            data: row,
            customer: customerDetails,
            customer1: customerDetails.cust1,
            instDetails: institutionDetails,
            footer: dataFrmXml,
            dataset: DatasetDoCalculated,
            bankLogo: bankLogo,
            currentDate: currentDate
          });

        var frmDt = customerDetails.cust1.startDate.split(' ').join('_');
        var toDt = customerDetails.cust1.endDate.split(' ').join('_');
        var accNo = customerDetails.cust1.accountNo;
        var stateNo = customerDetails.cust1.stmtNo;

        //  console.log("frmDt", frmDt, "toDt", toDt );

        var pdfName = accNo + '_' + frmDt + '-' + toDt + '_' + stateNo;

        console.log(new Date().toISOString(), "BEGIN PDF");
        var pdfPath = '/opt/ss/smart-statement/public/pdfDownload/' + session + '_' + pdfName + '.pdf'
        pdf.create(sampleHtml, options).toFile('/opt/ss/smart-statement/public/pdfDownload/' + session + 'encrypt.pdf', function (err, res1) {
          console.log('PDF', err, res1);

          if (err) {
            console.log("pdf err", err);
          }
          var fileSize = getFileSize('/opt/ss/smart-statement/public/pdfDownload/' + session + 'encrypt.pdf');
          console.log('PDF FILE SIZE------------->>', fileSize + ' MB')
          const pdfDoc = new HummusRecipe('/opt/ss/smart-statement/public/pdfDownload/' + session + 'encrypt.pdf', pdfPath);

          // pdf encryption with customerId as password
          pdfDoc
            .encrypt({
              userPassword: custId.toString(),
              ownerPassword: custId.toString(),
              userProtectionFlag: 4
            })
            .endPDF();
          console.log(new Date().toISOString(), "END PDF");
          console.log("pdf path-->>>", __dirname, '/public/pdfDownload/' + session + '_' + pdfName + '.pdf');
          var file = __dirname + '/public/pdfDownload/' + session + '_' + pdfName + '.pdf';
          //   fs.readFile( "/home/t24smart/7146515_01_AUG_2019-06_AUG_2019_39.pdf" , function (err,data){
          //     res.contentType("application/pdf");
          //     res.send(data);
          // });
          console.log("4");
          res.status(200).send({
            status: true,
            fileName: 'http://' + req.get('host') + '/tempfile/' + session + '_' + pdfName + '.pdf'
          });

        });
        
      });

    }

  });

});


//Listen port
let port = 8013;


//application starting
var server = app.listen(port, () => {
  console.log('Server is up and running on port numner ' + port);
});

//server.setTimeout(5000000)






